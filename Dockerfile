FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest
LABEL maintainer="cloud-infrastructure-3rd-level@cern.ch"

ENV SHELL=bash
ENV OS_CLOUD=cern

RUN mkdir /etc/openstack
COPY clouds.yaml /etc/openstack

COPY cloud-openstack.repo /etc/yum.repos.d/

RUN dnf install -y python3-openstackclient python3-requests-kerberos \
    python3-manilaclient python3-octaviaclient python3-ironicclient \
    python3-barbicanclient git jq && \
    dnf clean all

CMD /bin/bash
